package com.example

import com.google.gson.Gson
import io.ktor.application.ApplicationCall
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.request.get
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readText
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import kotlinx.coroutines.experimental.runBlocking


fun professorHandler(call: ApplicationCall, params: Map<String, String>) {
    val client = HttpClient(Apache) {
        //configuration
    }
    runBlocking {
        when {
            (params["surname"] == Error.UndefinedProfessorSurname.message) -> {
                call.respond(HttpStatusCode.OK, mapOf(
                        "message" to Error.UndefinedProfessorSurname.message
                ))
            }
            (params["surname"]!!.split(",").size > 1) -> {
                val professors= mutableListOf<Professor>()

                params["surname"]!!.split(",").toSet().forEach {professorSurname->
                    val response = client.get<HttpResponse>("${call.application.BD_SERVER_URL}/$urlPathGetProfessors/$professorSurname" +
                            "?" +
                            if(params["days"]?.isNotEmpty() == true) "days=${params["days"]}" else ""
                    )
                    val responseText=response.readText()
                    professors.addAll(deserializationArrayFromGsonProfessor(responseText))
                }
                val jsonBody=Gson().toJson(professors)
                call.respond(HttpStatusCode.OK, TextContent(
                        "{" +
                                "\"message\": \" " +
                                    "${Error.ManyProfessorsFoundedFirst.message}\n" +
                                    "${professors.joinToString("\n") { "${professors.indexOf(it) + 1} - ${it.surname} ${it.name} ${it.patronymic}" }}\n" +
                                    "${Error.ManyProfessorsFoundedSecond.message}"+
                                "\","+
                                "\"body\" : $jsonBody,"+
                                "\"continue_dialog_on_client\" : true"+
                            "}"
                        ,ContentType.Application.Json))

            }
            else -> {
                val response = client.get<HttpResponse>("${call.application.BD_SERVER_URL}/$urlPathGetProfessors/${params["surname"]}" +
                        "?" +
                        if(params["days"]?.isNotEmpty() == true) "days=${params["days"]}" else ""
                )
                call.respond(response.status, TextContent(
                        "{"+
                                "\"body\":"+
                                response.readText()+
                            "}"
                    ,ContentType.Application.Json))
            }
        }
    }

}

fun jokeHandler(call: ApplicationCall, params: Map<String, String>) {
    val client = HttpClient(Apache) {
        //config
    }
    runBlocking {
        when (params["theme"]) {
            Error.UndefinedJokeTheme.message -> {
                call.respond(HttpStatusCode.BadRequest, mapOf(
                        "message" to Error.UndefinedJokeTheme.message
                ))
            }
            "" -> {
                val response = client.get<HttpResponse>("${call.application.BD_SERVER_URL}/$urlPathGetJokes")
                call.respond(response.status, response.readText())
            }
            else -> {
                val response = client.get<HttpResponse>("${call.application.BD_SERVER_URL}/$urlPathGetJokes/${params["theme"]}")
                call.respond(response.status, response.readText())
            }
        }
    }
}