package com.example

import io.ktor.application.Application
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.features.json.GsonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.get
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readText
import kotlinx.coroutines.experimental.runBlocking
import java.io.File


fun messageParamAnalyze(message: String, clazz: Classifier, application: Application): Map<String, String> {
    //Here you can change a comparator
    val comparator=LevenshteinComparator() as IWordComparator
    val exceptionWords=File(pathToExceptionWords).readLines()

    val params = mutableMapOf<String, String>()
    val client = HttpClient(Apache) {
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
    }
    val messageWord=message.trim().split(" ",",",";","!","?",":").filterNot { it.trim().isEmpty() }

    runBlocking {
        when (clazz) {
            Classifier.Professor -> {
                val dayOfWeekKeyWordsRus=File(pathToDayOfWeekKeyRusWords).readLines()
                val dayOfWeekKeyWordsEng=File(pathToDayOfWeekKeyEngWords).readLines()

                val response = try{
                    client.get<HttpResponse>("${application.BD_SERVER_URL}/$urlPathGetInfoProfessors")
                } catch(e:Exception){
                    throw DBServerConnectionException()
                }
                val professors = deserializationArrayFromGsonProfessor(response.readText())


                val professorSurnames = professors.filter { professor->
                    messageWord.filterNot{word->
                        exceptionWords.any { it==word }
                    }.any{ word->
                        comparator.compareTo(word,professor.surname, ProfessorLevensteinDistance)
                    }
                }.map { it.surname }

                //TODO Сделать общий интерфейс для фильтрации каких-либо параметров запроса
                val daysOfWeek=dayOfWeekKeyWordsRus.filter {day->
                    messageWord.filterNot{word->
                        exceptionWords.any { it==word }
                    }.any {word->
                        comparator.compareTo(word,day, DaysLevensteinDistance)
                    }
                }

                params["surname"] = if(professorSurnames.isNotEmpty()) professorSurnames.joinToString(",") else Error.UndefinedProfessorSurname.message
                params["days"]= daysOfWeek.joinToString(separator = ",") { day->
                    val index=dayOfWeekKeyWordsRus.indexOf(day)
                    dayOfWeekKeyWordsEng[index]
                }
            }
            Classifier.Joke -> {
                val jokeThemeKeyWords=File(pathToJokeThemeKeyWords).readLines()

                val response = client.get<HttpResponse>("${application.BD_SERVER_URL}/$urlPathGetInfoJokes")
                //TODO do only one func for deserializing from GSON to Array
                val jokes = deserializationArrayFromGsonJoke(response.readText())
                val jokeThemes= if(messageWord.any { word-> jokeThemeKeyWords.contains(word)}){
                    jokes.find {joke->
                        messageWord.any {word->
                            comparator.compareTo(word,joke.theme, JokesLevensteinDistance)
                        }
                    }?.theme?:Error.UndefinedJokeTheme.message
                }
                else {
                    ""
                }

                params["theme"] = jokeThemes
            }
            Classifier.Undefined->{

            }
        }
    }
    return params
}

fun messageClassAnalyze(message: String): Classifier {
    val professorKeyWords = File(pathToProfessorKeyWords).readLines()
    val jokeKeyWord=File(pathToJokeKeyWords).readLines()

    val wordList = message.split(" ",",",";","!","?",":")
    val checkClassProfessor: Boolean = wordList.any { word ->
        professorKeyWords.any {
            word.contains(it, ignoreCase = true)
        }
    }
    val checkClassJoke: Boolean = wordList.any { word ->
        jokeKeyWord.any {
            word.contains(it, ignoreCase = true)
        }
    }

    return when {
        checkClassProfessor && !checkClassJoke -> Classifier.Professor
        !checkClassProfessor && checkClassJoke -> Classifier.Joke
        else -> Classifier.Undefined
    }

}