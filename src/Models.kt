package com.example

data class PostMessage (val message: String="", val clarify: Boolean=false)
data class Item (val key: String, val value: String)
data class Joke (val theme:String, val body:String)
data class Professor (val id:Long, val surname:String, val name:String, val patronymic: String, val chair: String, val schedule: Schedule){
    data class Schedule(val id:Long, val monday: List<Lesson>, val tuesday: List<Lesson>, val wednesday: List<Lesson>, val thursday: List<Lesson>, val friday: List<Lesson>, val saturday: List<Lesson>){
            data class Lesson(val time:String, val subject: Subject){
                data class Subject(val numerator:SubjectWeek, val denominator: SubjectWeek, val is_differ: Boolean){
                    data class SubjectWeek(val name: String, val auditory: String)
                }
            }
    }
}

data class MessageResponse(val message: String, val body: String, val continue_dialog_on_client:Boolean)