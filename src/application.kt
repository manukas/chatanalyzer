package com.example

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.features.UnsupportedMediaTypeException
import io.ktor.gson.gson
import io.ktor.http.HttpStatusCode
import io.ktor.request.ContentTransformationException
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import java.text.DateFormat

fun main(args: Array<String>) {
    io.ktor.server.netty.DevelopmentEngine.main(args)
}

fun Application.module() {
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }
    install(StatusPages) {
        exception<UndefinedClassException>{
            println(it.toString())
            call.respond(HttpStatusCode.OK, mapOf(
                    "message" to it.error.message
            ))
        }
        exception<UnsupportedMediaTypeException> {
            println(it.toString())
            call.respond(HttpStatusCode.OK, mapOf(
                    "message" to Error.EmptyBody.message
            ))
        }
        exception<ContentTransformationException> {
            println(it.toString())
            call.respond(HttpStatusCode.OK, mapOf(
                    "message" to Error.UndefinedClass.message
            ))
        }
        exception <DBServerConnectionException>{
            println(it.toString())
            call.respond(HttpStatusCode.OK, mapOf(
                    "message" to it.error.message
            ))
        }
    }

    routing {

        route("/api/v1") {
            post("/send_message") {

                val post = call.receive<PostMessage>()


                val clazz = messageClassAnalyze(post.message)
                if(clazz==Classifier.Undefined) throw UndefinedClassException()

                val params = messageParamAnalyze(post.message, clazz, call.application)

                when (clazz) {
                    Classifier.Professor -> professorHandler(call, params)
//                    Classifier.Joke -> jokeHandler(call, params)
                }
            }
        }
        get("/check") {
            // Check service
            call.respondText("OK")
        }
        get("/api") {
            //TODO открывать здесь документацию
        }
    }
}

