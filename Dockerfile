FROM gradle:jdk10 as builder

ENV USER_NONROOT $USER
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build

FROM openjdk:10-jre-slim
EXPOSE 8080

RUN mkdir /app
COPY --from=builder /home/gradle/src/build/libs/chat_analyzer.jar /app/

RUN mkdir /resources
COPY resources/dayOfWeekKeyEngWords.txt     resources/dayOfWeekKeyEngWords.txt
COPY resources/dayOfWeekKeyRusWords.txt     resources/dayOfWeekKeyRusWords.txt
COPY resources/exceptionWords.txt           resources/exceptionWords.txt
COPY resources/jokeKeyWords.txt             resources/jokeKeyWords.txt
COPY resources/professorKeyWords.txt        resources/professorKeyWords.txt
COPY resources/jokeThemeKeyWords.txt        resources/jokeThemeKeyWords.txt