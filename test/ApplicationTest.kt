import com.example.module
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import org.junit.Test
import io.ktor.application.Application
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ApplicationTest {
    @Test
    fun testRequest() = withTestApplication(Application::module) {

        with(handleRequest(HttpMethod.Get, "/check")) {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals("OK",response.content)
        }
        with(handleRequest(HttpMethod.Post, "/api/v1/send_message")) {
            assertTrue(requestHandled)
            println(response.content)
            assertTrue(response.content?.isNotEmpty()?:false)
        }
    }
}